package devops.katas.adminprovider;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
public class AdminController {

    private static HashMap<String, String> adminMap = new HashMap<String, String>(){
        {
            put("01", "jim");
            put("02", "solo");
        }
    };

    @GetMapping("/admin/{id}")
    public String getAdmin(@PathVariable String id) {

        return String.format("I am admin, id is %s", adminMap.get(id));
    }
}
