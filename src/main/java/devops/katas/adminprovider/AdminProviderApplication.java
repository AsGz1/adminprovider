package devops.katas.adminprovider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdminProviderApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdminProviderApplication.class, args);
	}

}
